<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'engW!]D 3%n,|B<f#A5zZaW<:sI#{xlWe7EOrP!uZJ*m(j;U/1%[zSy!hO,NJ%3Y');
define('SECURE_AUTH_KEY',  'pA}Lm~5*4Ok&1Z`PH%P9h_d[p)%49CvS7J0_7 nVYFdjlEh(h]yO6-GWq&xrU=E ');
define('LOGGED_IN_KEY',    'g[!NMSTAQ?d<7~x>Yfc6F@`!b(FZ4N^VZ][ ]Y5KJs1METV:(~}?B<I0~-FW#t?W');
define('NONCE_KEY',        ' O{V?ehsYAcU[6f7ANKL?QURZvu^HE ^4c<1gBHLPxJ)KfSr5J}1n|[+3#.a1m&1');
define('AUTH_SALT',        '{8Qf<S`x6]lvT8r!OMN8i0H:B:5FGLA:GJNi8uNB^t{w=U2;:rCz<Hxd14cRhiUL');
define('SECURE_AUTH_SALT', '!&(WI-PdgjFY #5Bhdb|Z8)9(J#S)rN*D|0]!Qy&spx:|P[5z~P4SR^V3Z;-^Drq');
define('LOGGED_IN_SALT',   '3%d+xI^y@;tJC(AB#5loXQnO8$D?SDW?5nx@s$GYhzdc.TT0sy9:aowE)Vh4/O$t');
define('NONCE_SALT',       'cB.7KGWO{PLN#_fyGq-wtpFxW{K$GW<4Tf<v%{v;Kys<da8v}Z]_$kPL.o1ErXw7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
